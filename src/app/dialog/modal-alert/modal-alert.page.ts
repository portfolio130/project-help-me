import { AlertService } from './../../alert.service';
import { Component, Input, OnInit } from '@angular/core';
import { Alert } from 'src/app/shared/alert';

@Component({
  selector: 'app-modal-alert',
  templateUrl: './modal-alert.page.html',
  styleUrls: ['./modal-alert.page.scss'],
})
export class ModalAlertPage implements OnInit {

  @Input() user: any;
  alertType: any[];
  
  constructor(public alertService: AlertService) {
    this.alertType = [{
      alertName: 'On me suit',
      alertLogo: 'Not yet'
    },
    {
      alertName: 'Je suis en danger chez moi',
      alertLogo: 'Not yet'
    },
    {
      alertName: 'Déclarer une zone dangeureuse',
      alertLogo: 'Not yet'
    },
    {
      alertName: 'Not yet implemented',
      alertLogo: 'Not yet'
    }];
  }

  ngOnInit() {
  }

  sendNotificationToUsers(event) {
    //TODO : IMPLEMENTER LA FONCTION QUI PERMET DE CIBLER LES UTILISATEURS A PROXIMITE
    console.log(event.target.childNodes[0].data);
    let alert: Alert;
    console.log(Date.now);

    switch (event.target.childNodes[0].data) {
      case 'Not yet':
        console.log('On a cliqué sur not yet');

        alert = {
          date: (new Date()).getTime() / 1000,
          title: 'On me suit',
          userId: this.user.uid
        };
        console.log(alert);
        this.alertService.registerAlert(alert);
        break;
      case 'Je suis en danger chez moi':
        console.log('On a cliqué sur Je suis en danger chez moi');
        alert = {
          date: (new Date()).getTime() / 1000,
          title: 'Je suis en danger chez moi',
          userId: this.user.uid
        };
        console.log(alert);
        this.alertService.registerAlert(alert);

        break;
    }
  }
}
