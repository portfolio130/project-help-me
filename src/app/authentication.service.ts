import { AngularFireDatabase, } from '@angular/fire/database';
import { Injectable, NgZone } from '@angular/core';

import { auth } from 'firebase/app';
import { User } from './shared/auth';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { ToastController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins;
@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  userData: any;


  constructor(
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    private db: AngularFirestore,
    public toastController: ToastController
  ) {
    this.ngFireAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });

  }

  // Login in with email/password
  SignIn(email, password) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password);
  }

  // Register user with email/password
  async RegisterUser(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password);
  }

  async SendVerificationMail() {
    return (await this.ngFireAuth.currentUser).sendEmailVerification()
      .then(() => {
        this.router.navigate(['verify-email']);
      });
  }

  // Recover password
  PasswordRecover(passwordResetEmail) {
    return this.ngFireAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        this.presentToast();
        this.router.navigateByUrl('/login');
      }).catch((error) => {
        window.alert(error);
      });
  }

  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // Returns true when user's email is verified
  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user.emailVerified !== false) ? true : false;
  }

  // Sign in with Gmail
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  // Auth providers
  AuthLogin(provider) {
    return this.ngFireAuth.signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['tabs']);
        });
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error);
      });
  }

  // Store user in localStorage
  SetUserData(user) {
    // const userRef = this.afStore.collection('users/').doc(user.uid);
    const userRef = this.afStore.doc(`users/${user.uid}`);
   
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      latitude: null,
      longitude: null
    };
    userRef.set(userData, {
      merge: true
    });
  }

  //  updateUser(user) {
  //   const dbRef = firebase.database().ref('users/');
  //   dbRef.on("child_changed", snap => {
  //     console.log(snap.val()); // will return updated user object
  // });
  // }

  // Sign-out 
  SignOut() {
    return this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }

  // getUserList(): FirebaseListObservable<User[]>{
  //   if(!this.userData) return;
  //   this.userData = this.db.list(`users/${this.userData.uid}`)
  //   return this.userData;
  // }

  pushUser(user) {
    var database = firebase.database();
    database.ref(`users/${user.uid}`).set({
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      latitude: null,
      longitude: null
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Votre mot de passe a été réinitialisé, consultez votre boite mail',
      duration: 2000
    });
    toast.present();
  }

}
