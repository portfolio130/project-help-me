import { UploadImageService } from './../upload-image.service';
import { User } from './../shared/auth';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as firebase from 'firebase';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

import { fileURLToPath } from 'url';
import { cpuUsage } from 'process';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {


  user;
  profilePictureAsChanged = false;
  file: File;
  storageRef = firebase.storage().ref();
  ref;
  task;
  uploadProgress;
  downloadURL;
  constructor(private angularFireAuth: AngularFireAuth,
              public authService: AuthenticationService, public router: Router,
    ) {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    


  }

  ngOnInit() {
    console.log(this.user.email);
    // console.log(this.user.photoURL);
    // setTimeout(() => console.log(firebase.auth().currentUser), 10000);

  }

  upload(event){
    // this.uploadImageService.handleFiles(event);
    // this.uploadImageService.uploadFile();
  }

}
