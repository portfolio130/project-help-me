import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: 'login',
  loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
},
{
  path: 'registration',
  loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationPageModule)
},
{
  path: 'verify-email',
  loadChildren: () => import('./verify-email/verify-email.module').then(m => m.VerifyEmailPageModule)

},
{
  path: 'tabs',
  loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
},
{
  path: '',
  redirectTo: 'login',
  pathMatch: 'full'
},
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'modal-alert',
    loadChildren: () => import('./dialog/modal-alert/modal-alert.module').then( m => m.ModalAlertPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
