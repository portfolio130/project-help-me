import { User } from './../shared/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  user : User;
  constructor(
    public authService: AuthenticationService,
    public router: Router ,
    private db: AngularFireDatabase  
  ) { }

  ngOnInit() { }

  async signUp(email, password) {
    console.log(email.value, password.value);
    await this.authService.RegisterUser(email.value, password.value).then((user) => {
      user.user.sendEmailVerification();
      console.log(user);
  
      this.user = {
        email : user.user.email,
        uid : user.user.uid,
        displayName: user.user.displayName,
        emailVerified: user.user.emailVerified,
        photoURL : user.user.photoURL,
        latitude: null,
        longitude: null
      },
  
  
      this.router.navigate(['verify-email']);
    }).catch((error) => {
      window.alert(error.message);
    });
  }

  backToLogin(){
    this.router.navigateByUrl('/login');
  }

  // async signUp(email, password) {
  //   console.log(email.value, password.value);
  //   setTimeout(async () => await this.authService.RegisterUser(email.value, password.value).then((user) => {
  //     user.user.sendEmailVerification();
  //     // console.log("coucou");
  //     // this.user = {
  //     //   email : user.user.email,
  //     //   uid : user.user.uid,
  //     //   displayName: user.user.displayName,
  //     //   emailVerified: user.user.emailVerified,
  //     //   photoURL : user.user.photoURL,
  //     //   latitude: null,
  //     //   longitude: null
  //     // },
  //     // this.authService.SetUserData(this.user);
  //     // console.log("coucou");
  //     this.router.navigate(['verify-email']);
  //   }).catch((error) => {
  //     window.alert(error.message);
  //   }), 5000 );
  // }



}
