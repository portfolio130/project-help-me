import { ModalAlertPage } from './../dialog/modal-alert/modal-alert.page';
import { GeoPos } from './../shared/geopos';

import { UserService } from './../user.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins;
const { Device } = Plugins;
import * as firebase from 'Firebase';
import { Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { GeoFire } from 'geofire';
declare var google: any;

/*
Source : https://www.djamware.com/post/5a48517280aca7059c142972/ionic-3-angular-5-firebase-and-google-maps-location-tracking
*/

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers = [];

  geoposition = {} as GeoPos;
  geoposRef;
  coords: any;
  isTracking = false;
  device = Device.getInfo();
  user = JSON.parse(localStorage.getItem('user'));
  ref = firebase.database().ref('geolocations/');
  geoFire = new GeoFire(this.ref);

  constructor(public platform: Platform, public router: Router,
    public modalController: ModalController) {

    platform.ready().then(() => {
      this.initMap();
    });
    this.ref.on('value', resp => {
      this.deleteMarkers();
      console.log("delete_markers");
    });
  }

  //LE MAPPING PREND AUSSI EN COMPTE L'UTILISATEUR COURANT, C'EST UNE ERREUR
  getClosestUsers(latitude, longitude, map) {
    const geoFire = new GeoFire(this.ref);
    const geoQuery = geoFire.query({
      center: [latitude, longitude],
      radius: 1
    });
    var markers = {};
    // tslint:disable-next-line: only-arrow-functions
    geoQuery.on('key_entered', function (key, location, distance) {
      console.log('User ' + key + ' found at ' + location + ' (' + distance + ' km away)');
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(location[0], location[1]),
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
      });
      markers[key] = marker;
    });
    geoQuery.on('key_moved', function (key, location, dist) {
      if (markers[key]) {
        markers[key].setMap(null);
        delete markers[key];
      }
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(location[0], location[1]),
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
      });
      markers[key] = marker;
    });
    geoQuery.on('key_exited', function (key, location, dist) {
      if (markers[key]) {
        markers[key].setMap(null);
        delete markers[key];
      }
    });
    this.setMapOnAll(map);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalAlertPage,
      cssClass: 'modal-alert',
      componentProps: {
        user: firebase.auth().currentUser

      },
      backdropDismiss: true,
      showBackdrop: true
    });
    return await modal.present();
  }

  ngOnInit(): void {
  }

  stopGoogleGeolocation(identifier) {
    Geolocation.clearWatch(identifier);
  }
  // TESTS COPIÉS
  initMap() {
    let watch;
    Geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true }).then((resp) => {
      const mylocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      console.log(Geolocation.getCurrentPosition());
      this.getClosestUsers(resp.coords.latitude, resp.coords.longitude, this.map);

      this.geoFire.set(
        this.user.uid,
        [resp.coords.latitude, resp.coords.longitude]
      );
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 15,
        center: mylocation
      });
    });
    return watch = Geolocation.watchPosition({}, (position, err) => {
      this.deleteMarkers();

      const updatelocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);


      // this.addMarker(updatelocation, 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
      this.getClosestUsers(position.coords.latitude, position.coords.longitude, this.map);
      this.geoFire.set(
        this.user.uid,
        [position.coords.latitude, position.coords.longitude]
      );
      this.setMapOnAll(this.map);
      console.log(this.map);

    });
  }

  addMarker(location, image) {
    const marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: image
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
    console.log(this.markers);

  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }
}

