import { LoginPage } from './../login/login.page';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit {
  isCreated = false;
  constructor(public authService: AuthenticationService, public loginPage: LoginPage) { }



  ngOnInit() {
    // this.sendUserToDb();
  }

  sendUserToDb() {
    let currentUser = firebase.auth().currentUser;
    console.log(currentUser);
    this.authService.pushUser(currentUser);
    this.isCreated = true;
  }

  callAll() {
    this.sendUserToDb();
    // this.loginPage.reloadPage();
  }



}
