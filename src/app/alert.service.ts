import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  alertRef = firebase.database().ref('alerts/');
  constructor() { }


  registerAlert(alert){
    this.alertRef.push(alert).key;
    console.log(this.alertRef.key);
    
  }
}
