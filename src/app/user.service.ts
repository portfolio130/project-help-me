import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from './models/user';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  // findUsers(): Observable<any> {
  //   return this.http.get(environment.baseUrl + '/api/users/getAllUsers');
  // }

  // saveUser(user: any): Observable<any>{
  //   return this.http.post(environment.baseUrl + '/api/users/save', user);
  // }

  getCurrentUserFromDb(user){
    var database= firebase.database();
    database.ref(`/users/${user.uid}`);
  }
}
