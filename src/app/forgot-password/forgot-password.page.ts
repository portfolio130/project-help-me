import { LoginPage } from './../login/login.page';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  constructor(public router: Router, public authService: AuthenticationService,
              public toastController: ToastController, public loginPage: LoginPage) { }

  ngOnInit() {
  }

  backToLogin() {
    this.router.navigateByUrl('/login');
  }



}
