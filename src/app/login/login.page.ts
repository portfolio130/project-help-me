import { VerifyEmailPage } from './../verify-email/verify-email.page';
import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { ModalController } from '@ionic/angular';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  mySubscription: any;
  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public modalController: ModalController
  ) {

  }

  ngOnInit() {

  }

  openForgotPwdPage() {
    this.router.navigateByUrl('/forgot-password');
  }

  reloadPage() {
    window.location.replace("login");
  }

  async logIn(email, password) {
    await this.authService.SignIn(email.value, password.value)
      .then((res) => {
       
        res.user.reload();
        if (res.user.emailVerified) {
          const currentUser = firebase.auth().currentUser;
          this.authService.SetUserData(currentUser);
          
          this.router.navigate(['tabs']);
        } else {
          window.alert('Email is not verified');
          return false;
        }
      }).catch((error) => {
        window.alert(error.message);
      });
  }

}
