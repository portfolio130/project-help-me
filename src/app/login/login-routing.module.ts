import { RegistrationPage } from './../registration/registration.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPage } from './login.page';
import { VerifyEmailPage } from '../verify-email/verify-email.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPage,
    children: [{
      path: 'forgot-password',
      loadChildren: () => import('../forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
    }

    ]
  },
  // {
  //   path: 'registration',
  //   loadChildren: () => import('../registration/registration.module').then(m => m.RegistrationPageModule)
  // },
  // {
  //   path: 'verify-email',
  //   loadChildren: () => import('../verify-email/verify-email.module').then(m => m.VerifyEmailPageModule)
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule { }
