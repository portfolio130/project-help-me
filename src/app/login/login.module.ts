import { ForgotPasswordPage } from './../forgot-password/forgot-password.page';
import { VerifyEmailPage } from './../verify-email/verify-email.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LoginPage],
  providers: [
    VerifyEmailPage
  ]
})
export class LoginPageModule {}
