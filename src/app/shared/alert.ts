export interface Alert {
    userId: string;
    title: string;
    date;
}
