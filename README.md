# Project Help me

The purpose of this application was to reduce the number of assaults on women, especially in the evening in the street.

The basic operation is as follows:

- I am in a dangerous situation

- I issue an alert according to my situation

- The alert and my position are sent to all people within 200 meters around me in order to get help

# Due to API keys obsolescence, this project is no longer working but it can serve as fundation for those who want to push it further
